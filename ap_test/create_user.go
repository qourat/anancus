package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/go-fed/activity/vocab"
)

func main() {
	create := &vocab.Create{}
	actor, err := url.Parse("http://localhost:8000/activity/application")
	if err != nil {
		log.Fatalf("could not parse actor url:", err)
	}
	create.AppendActorIRI(actor)

	inbox := &vocab.OrderedCollection{}
	outbox := &vocab.OrderedCollection{}
	following := &vocab.Collection{}
	followers := &vocab.Collection{}
	liked := &vocab.Collection{}
	likes := &vocab.Collection{}

	id, err := url.Parse("http://localhost:8000/activity/person/joe")
	if err != nil {
		log.Fatalf("could not parse actor url:", err)
	}

	person := &vocab.Person{}
	person.SetId(id)
	person.SetInboxOrderedCollection(inbox)
	person.SetOutboxOrderedCollection(outbox)
	person.SetFollowingCollection(following)
	person.SetFollowersCollection(followers)
	person.SetLikedCollection(liked)
	person.SetLikesCollection(likes)

	create.AppendObject(person)

	m, err := person.Serialize()
	if err != nil {
		log.Fatalf("failed to serialize:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("could not marshal json:", err)
	}
	fmt.Println(string(b))
	postUrl := "http://localhost:8000/activity/application/outbox"
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatalf("could not post outbox:", err)
	}
	req.Header.Set("Host", "localhost")
	req.Header.Set("Authorization", "Bearer XXXXXXXXXXX")
	req.Header.Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}
	fmt.Println(string(dump))
}
